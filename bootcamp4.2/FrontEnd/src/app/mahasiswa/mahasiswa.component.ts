import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mahasiswa',
  templateUrl: './mahasiswa.component.html',
  styleUrls: ['./mahasiswa.component.css']
})
export class MahasiswaComponent implements OnInit {nama:string = "";
  alamat:string = "";
  email:string = "";
  phone:string = "";
  // confirm:string = "";

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  register(){
    this.api.register({
        'nama' : this.nama,
        'alamat' : this.alamat,
        'email' : this.email,
        'phone' : this.phone
    });
  }
}
