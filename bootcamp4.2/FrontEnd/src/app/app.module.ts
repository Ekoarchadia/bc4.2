import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ApiService } from './services/api.service';

import { AppComponent } from './app.component';
import { CourseListComponent } from './course-list/course-list.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { MahasiswaComponent } from './mahasiswa/mahasiswa.component';

@NgModule({
  declarations: [
    AppComponent,
    CourseListComponent,
    AssignmentComponent,
    MahasiswaComponent
  ],
  imports: [
    BrowserModule,FormsModule, HttpModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
