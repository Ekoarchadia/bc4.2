import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  nama_course:string = "";
  nama_dosen:string = "";
  sks:string = "";

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  register(){
    this.api.register({
        'nama_course' : this.nama_course,
        'nama_dosen' : this.nama_dosen,
        'sks' : this.sks
    });
  }

}
