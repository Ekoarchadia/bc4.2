<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class mahasiswaController extends Controller
{
    public function getData(){
        DB::beginTransaction();
        try
        {
            $this->validate($request,[
                'nama' => 'required',
                'email' => 'required|email'
            ]);
            $mahasiswa = mahasiswa::get();
            DB::commit();
            return response()->json($mahasiswa, 201);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    public function addData(Request $request){
        DB::beginTransaction();

        try{
            $this->validate($request,[
                'nama' => 'required',
                'email' => 'required|email'
            ]);
                $newMahasiswa = new Mahasiswa;
                $newMahasiswa->nama = $request->input('nama');
                $newMahasiswa->alamat = $request->input('alamat');
                $newMahasiswa->email = $request->input('email');
                $newMahasiswa->phone = $request->input('phone');
                $newMahasiswa->save();

                DB::commit();
                return response()->json(["message"=>"Success"], 201);
            }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }
    public function updateData(Request $request)
    {
        DB::beginTransaction();
        try{
                    $this->validate($request,[
                        'id' => 'required',
                    ]);
            $mahasiswa = Mahasiswa::find(2);
            $mahasiswa->nama = 'Ariefn';
            $mahasiswa->alamat = 'Meruya';
            $mahasiswa->email = 'arief@gmail.com';
            $mahasiswa->phone = '08782198417';
            $mahasiswa->save();
            DB::commit();
                        return response()->json(["message"=>"Success"], 201);
            }
            
        catch(\Exception $e){
                DB::rollBack();
                return response()->json(["message"=> $e->getMessage ], 500);
            }
    }

    public function deleteData(Request $request){
        DB::beginTransaction();

        try{
            $this->validate($request,[
                'id' => 'required',
            ]);
                $data = mahasiswa::find((integer)$request->input("id"));
                if(empty($data))
                {
                    return response()->json(["message"=>"Not Found"], 404);
                }

                $data->delete();
                DB::commit();

                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
             DB::rollBack();
            return response()->json(["message"=> $e->getMessage], 500);
        }
            
    }
}
